from django import forms

from .models import SignUp

class ContactForm( forms.Form ):
    full_name = forms.CharField( required = False )
    email = forms.EmailField()
    message = forms.CharField()

class SignUpForm( forms.ModelForm ):
    class Meta:
        model = SignUp
        fields = [ 'full_name', 'email' ]
        ### exclude = [ 'full_name' ] use sparingly

    def clean_email( self ):
        email = self.cleaned_data.get( 'email' )
        email_base, provider = email.split( "@" )
        # domain, n, extension = provider.split( '.' )
        emailList = email.split( '.' )
        extension = emailList[ -1 ]
        if not extension == "edu":
            raise forms.ValidationError( "Please use a valid .EDU email address" )
        return email

    def cleaned_full_name( self ):
        full_name = self.cleaned_data.get( 'full_name' )
        # write Validation code.
        return full_name
