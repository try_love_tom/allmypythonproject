# Import a library of functions called 'pygame'
import pygame
import random

# Define PI
PI = 3.14159

# Define some colors
BLACK   = (   0,   0,   0 )
WHITE   = ( 255, 255, 255 )
GREEN   = (   0, 255,   0 )
RED     = ( 255,   0,   0 )
BLUE    = (   0,   0, 255 )

# Initialize the game engine
pygame.init()

# Set the width and height of the screen [width, height]
size = ( 400, 400 )

# Opening and setting the window size
screen = pygame.display.set_mode( size )
pygame.display.set_caption( "Professor Tom's Cool Game" )

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# Create an empty array
snow_list = []

# Loop 50 times and add a snow flake in a random x,y position
for i in range( 50 ):
    x = random.randrange( 0, 400 )
    y = random.randrange( 0, 400 )
    snow_list.append( [ x, y ] )

# ------ Main Program Loop ------
while not done:
# --- Main event loop
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # IF user clicked close
            done = True # Flag that we are done so we exit this loop
            print( "User asked to quit." )
        elif event.type == pygame.KEYDOWN:
            print( "User pressed a key." )
        elif event.type == pygame.KEYUP:
            print( "User let go of a key." )
        elif event.type == pygame.MOUSEBUTTONDOWN:
            print( "User pressed a mouse button" )
    # --- Game logic should go here

    # --- Drawing code should go here

    # All drawing code happens after the for loop and but
    # inside the main while not done loop.

    # Clear the screen and set the screen background
    screen.fill( BLACK )

    # Process each snow flake in the list
    for i in range( len ( snow_list ) ):
    
        # Draw the snow flake
        pygame.draw.circle( screen, WHITE, snow_list[ i ], 2 )
        
        # Move the snow flake down one pixel
        snow_list[ i ][ 1 ] += 1
        
        # If the snow flake has moved off the bottom of the screen
        if snow_list[ i ][ 1 ] > 400:
            # Reset it just above the top
            y = random.randrange( -50, -10 )
            snow_list[ i ][ 1 ] = y
            # Give it a new x position
            x = random.randrange( 0, 400 )
            snow_list[ i ][ 0 ] = x
        pygame.draw.circle( screen, WHITE, [ x, y ], 2 )

    # Go ahead and update the screen with what we've drawn.
    # This MUST happen after all the other drawing commands.
    pygame.display.flip()
    
    # This limits the while loop to a max of 60 times per second.
    # Leave this out and we will use all CPU we can.
    clock.tick( 60 )

# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()