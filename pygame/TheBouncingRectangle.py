# Import a library of functions called 'pygame'
import pygame
import math

# Define PI
PI = 3.14159

# Define some colors
BLACK   = (   0,   0,   0 )
WHITE   = ( 255, 255, 255 )
GREEN   = (   0, 255,   0 )
RED     = ( 255,   0,   0 )
BLUE    = (   0,   0, 255 )

# Initialize the game engine
pygame.init()

# Set the width and height of the screen [width, height]
size = ( 700, 500 )

# Opening and setting the window size
screen = pygame.display.set_mode( size )
pygame.display.set_caption( "Professor Tom's Cool Game" )

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# Using variables to store object speed
# Starting position of the rectangle
rect_x = 50
rect_y = 50

# Speed and direction of rectangle
rect_change_x = 5
rect_change_y = 5

# ------ Main Program Loop ------
while not done:
# --- Main event loop
    for event in pygame.event.get(): # User did something
        if event.type == pygame.QUIT: # IF user clicked close
            done = True # Flag that we are done so we exit this loop
            print( "User asked to quit." )
        elif event.type == pygame.KEYDOWN:
            print( "User pressed a key." )
        elif event.type == pygame.KEYUP:
            print( "User let go of a key." )
        elif event.type == pygame.MOUSEBUTTONDOWN:
            print( "User pressed a mouse button" )
    # --- Game logic should go here

    # --- Drawing code should go here

    # All drawing code happens after the for loop and but
    # inside the main while not done loop.

    # Clear the screen and set the screen background
    screen.fill( BLACK )

    # Draw the rectangle
    pygame.draw.rect( screen, WHITE, [ rect_x, rect_y, 50, 50 ] )
    
    # Move the rectangle starting point
    rect_x += rect_change_x
    rect_y += rect_change_y

    # Bounce the rectangle if needed
    if rect_y > 450 or rect_y < 0:
        rect_change_y = rect_change_y * -1
    if rect_x > 650 or rect_x < 0:
        rect_change_x = rect_change_x * -1

    # Go ahead and update the screen with what we've drawn.
    # This MUST happen after all the other drawing commands.
    pygame.display.flip()
    
    # This limits the while loop to a max of 60 times per second.
    # Leave this out and we will use all CPU we can.
    clock.tick( 60 )

# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()