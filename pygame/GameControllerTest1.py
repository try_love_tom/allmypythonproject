"""
    Show how to use a sprite backed by a graphic.
    
    Sample Python/Pygame Programs
    Simpson College Computer Science
    http://programarcadegames.com/
    http://simpson.edu/computer-science/
    
    Explanation video: http://youtu.be/vRB_983kUMc
    """

import pygame
import math

# Define some colors
BLACK   = ( 0,     0,   0 )
WHITE   = ( 255, 255, 255 )
RED     = ( 255,   0,   0 )
GREEN   = ( 0,   255,   0 )
BLUE    = ( 0,     0, 255 )

def draw_snowman( screen, x, y ):
    # Draw a circle for the head
    pygame.draw.ellipse( screen, WHITE, [ 35 + x, 0 + y, 25, 25 ] )
    # Draw the middle snowman circle
    pygame.draw.ellipse( screen, WHITE, [ 23 + x, 20 + y, 50, 50 ] )
    # Draw the bottom snowman circle
    pygame.draw.ellipse( screen, WHITE, [ 0 + x, 65 + y, 100, 100 ] )

def draw_stick_figure( screen, x, y ):
    # Head
    pygame.draw.ellipse( screen, BLACK, [ 1 + x, y, 10, 10 ], 0 )
    # Legs
    pygame.draw.line( screen, BLACK, [ 5 + x, 17 + y ], [ 10 + x, 27 + y ], 2 )
    pygame.draw.line( screen, BLACK, [ 5 + x, 17 + y ], [ x, 27 + y ], 2 )
    # Body
    pygame.draw.line(screen, RED, [ 5 + x, 17 + y ], [ 5 + x, 7 + y ], 2 )
    # Arms
    pygame.draw.line(screen, RED, [ 5 + x, 7 + y ], [ 9 + x, 17 + y ], 2 )
    pygame.draw.line(screen, RED, [ 5 + x, 7 + y ], [ 1 + x, 17 + y ], 2 )

pygame.init()

# Set the width and height of the screen [width, height]
size = ( 700, 500 )
screen = pygame.display.set_mode( size )

pygame.display.set_caption( "My Game" )

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# keyboard
# Speed in pixels per frame
x_speed = 0
y_speed = 0

# joystick
# Current position
x_coord = 10
y_coord = 10

# Count the joysticks the computer has
joystick_count = pygame.joystick.get_count()
if joystick_count == 0:
    # No joysticks!
    print("Error, I didn't find any joysticks.")
else:
    # Use joystick #0 and initialize it
    my_joystick = pygame.joystick.Joystick(0)
    my_joystick.init()


# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here

        # User pressed down on a key
        """if event.type == pygame.KEYDOWN:
            # Figure out if it was an arrow key. If so
            # adjust speed.
            if event.key == pygame.K_LEFT:
            x_speed = -3
            if event.key == pygame.K_RIGHT:
            x_speed = 3
            if event.key == pygame.K_UP:
            y_speed = -3
            if event.key == pygame.K_DOWN:
            y_speed = 3"""
        
        # User let up on a key
        """if event.type == pygame.KEYUP:
            # If it is an arrow key, reset vector back to zero
            if event.key == pygame.K_LEFT:
            x_speed = 0
            if event.key == pygame.K_RIGHT:
            x_speed = 0
            if event.key == pygame.K_UP:
            y_speed = 0
            if event.key == pygame.K_DOWN:
            y_speed = 0"""

        # As long as there is a joystick
        if joystick_count != 0:
            
            # This gets the position of the axis on the game controller
            # It returns a number between -1.0 and +1.0
            horiz_axis_pos = my_joystick.get_axis( 0 )
            vert_axis_pos = my_joystick.get_axis( 1 )
            
            # Move x according to the axis. We multiply by 10 to speed up the movement.
            # Convert to an integer because we can't draw at pixel 3.5, just 3 or 4.
            # x_coord = x_coord + int( horiz_axis_pos * 10 )
            # y_coord = y_coord + int( vert_axis_pos * 10 )
            if ( horiz_axis_pos < 0 ):
                x_speed = -int( math.exp( -horiz_axis_pos * 2 ) - 1 )
            else:
                x_speed = int( math.exp( horiz_axis_pos * 2 ) - 1 )
            
            if ( vert_axis_pos < 0 ):
                y_speed = -int( math.exp( -vert_axis_pos * 2 ) - 1 )
            else:
                y_speed = int( math.exp( vert_axis_pos * 2 ) - 1 );

    # Game logic
    # mouse
    """pos = pygame.mouse.get_pos()
    x = pos[0]
    y = pos[1]"""
    
    # Move the object according to the speed vector.
    print 'pos( ' + str( x_coord ) + ', ' + str( y_coord ) + ' )'
    
    if ( ( x_coord + x_speed < size[ 0 ] ) and ( x_coord + x_speed > 0 ) ):
        x_coord += x_speed
    if ( ( y_coord + y_speed < size[ 1 ] ) and ( y_coord + y_speed > 0 ) ):
        y_coord += y_speed


    # --- Drawing code should go here

    # First, clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.
    screen.fill( WHITE )

    # Drawing section
    # draw_stick_figure(screen, x, y)

    # Draw the stick figure
    draw_stick_figure( screen, x_coord, y_coord )


    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()
    
    # --- Limit to 60 frames per second
    clock.tick( 60 )

# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()