import serial
import time

port = serial.Serial( "/dev/tty.SLAB_USBtoUART", baudrate = 9600, timeout = 3.0 )

while True:
    string = port.readline()
    if string == "":
        string = "None"
    t = time.time()
    print time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(t)) + "\tReceive: " + string
    port.write( string )
